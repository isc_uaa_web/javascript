//esperar a que cargue la pagina
$(document).ready(function () {
    //pedir informacion a servlet usando ajax
    //las llamadas ajax son asncrinonas (ejecutadas en segundo plano) por lo que no bloquean la interfaz grafica
    $.ajax({
        //url que esta escuchando tu servlet que respondera con un json
        'url': 'articulo',
        //funcion que se ejecutara una vez que el servidor responda correctamente
        'success': function (result) {
            $('#nombre').val(result.nombre);
            $('#cantidad').val(result.cantidad);
            $('#precio').val(result.precio);
            $('#disponible').prop('checked', result.disponible);
        },
        //funcion a ejecutarse una vez que el servidor falle
        'failure': function (data) {
            alert('algo fallo!');
        }
    });
});