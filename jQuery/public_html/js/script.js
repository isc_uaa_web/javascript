
//document ready will wait for DOM to complete load
$(document).ready(function () {

    //hide clicked paragraph
    //tag selector will get all tags in document
    $('p').click(function () {
        $(this).hide();
    });

    //hide all paragraphs
    //id selector to get only an specific element
    $("#hide-pagraphs").click(function () {
        $("p").hide(1000);
    });

    $('#hide-heading').click(function () {
        //class selector to get all elements using a CSS class
        $('.heading').hide(1000, function () {
            alert('headings are hidden!');
        });
    });

    //id selector to get only an specific element
    $("#show-pagraphs").click(function () {
        $("p").show();
    });

    $('#hide-heading').click(function () {
        //class selector to get all elements using a CSS class
        $('.heading').show();
    });

    $("#fade-boxes").click(function () {
        $("#div1").fadeIn();
        $("#div2").fadeIn("slow");
        $("#div3").fadeIn(3000);
    });
});